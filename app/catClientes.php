<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class catClientes extends Model
{
    protected $fillable=['tipo'];

    public function clientes(){
    	return $this->hasMany('App\prospect','categorias_id','id');
    }

}
