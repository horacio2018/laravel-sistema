<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProdutosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produtos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->string('produto')->unique();
            $table->decimal('valor', 10, 2);
            $table->text('descricao');
            $table->unsignedBigInteger('categoria');
            $table->unique(array('produto','categoria'));
            $table->foreign('categoria')->references('id')->on('categorias_produtos')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('produtos');
    }
}
