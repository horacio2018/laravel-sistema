<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailProspectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('email_prospects', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();

            $table->unique(array('email','prospect'));

            $table->unsignedBigInteger('email');
            $table->foreign('email')->references('id')->on('emails')->onDelete('cascade');


            $table->unsignedBigInteger('prospect');
            $table->foreign('prospect')->references('id')->on('prospects')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('email_prospects');
    }
}
