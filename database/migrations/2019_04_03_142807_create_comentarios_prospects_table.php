<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComentariosProspectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comentarios_prospects', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->text('comentario');
            $table->unsignedBigInteger('id_prospect');
            $table->foreign('id_prospect')->references('id')->on('prospects')->onDelete('cascade');
            ///$table->foreign('usuario_pai')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comentarios_prospects');
    }
}
